using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza_zad1
{
    public partial class Form1 : Form
    {

        double a = 0;
        string operacija = "";
        bool isOperationPerformed = false;




        public Form1()
        {
            InitializeComponent();
        }




        private void Button_Click(object sender, EventArgs e)
        {
           
            if ((tbResult.Text == "0") || (isOperationPerformed))
                tbResult.Clear();

            isOperationPerformed = false;
            Button button = (Button)sender;
            if (button.Text == ".")
            {
                if (!tbResult.Text.Contains("."))
                    tbResult.Text = tbResult.Text + button.Text;

            }
            else
                tbResult.Text = tbResult.Text + button.Text;
        }

      

        private void Operatori(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (a != 0)
            {
                equals.PerformClick();
               operacija = button.Text;
                lDoSada.Text = a + " " + operacija;
                isOperationPerformed = true;
            }
            else
            {

                operacija = button.Text;
                a = Double.Parse(tbResult.Text);
                lDoSada.Text = a + " " + operacija;
                isOperationPerformed = true;
            }

        }

        private void equals_click(object sender, EventArgs e)
        {
           

            switch (operacija)
            {
                case "+":
                    tbResult.Text = (a + Double.Parse(tbResult.Text)).ToString();
                    break;
                case "-":
                    tbResult.Text = (a - Double.Parse(tbResult.Text)).ToString();
                    break;
                case "*":
                    tbResult.Text = (a * Double.Parse(tbResult.Text)).ToString();
                    break;
                case "/":
                    if (Double.Parse(tbResult.Text) == 0) {
                        tbResult.Text = "nedozvoljena operacija";
                        break;
                    }
                    else
                    tbResult.Text = (a / Double.Parse(tbResult.Text)).ToString();
                    break;
                default:
                    break;
            }
            a = Double.Parse(tbResult.Text);
            lDoSada.Text = "";
        }






        private void c_click(object sender, EventArgs e)
        {
            tbResult.Text = "0";
        }

        private void CE_click(object sender, EventArgs e)
        {
            tbResult.Text = "0";
            a = 0;
        }

        private void kvadriraj(object sender, EventArgs e)
        {
            tbResult.Text = (Double.Parse(tbResult.Text) * Double.Parse(tbResult.Text)).ToString();
            
        }

        private void sqrt_click(object sender, EventArgs e)
        {
            
            tbResult.Text = Math.Sqrt(Double.Parse(tbResult.Text)).ToString();
           
        }

        private void sin_click(object sender, EventArgs e)
        {
            tbResult.Text = Math.Sin(Double.Parse(tbResult.Text)).ToString();

        }

        private void cos_click(object sender, EventArgs e)
        {
            tbResult.Text = Math.Cos(Double.Parse(tbResult.Text)).ToString();

        }

        private void tg_click(object sender, EventArgs e)
        {
            tbResult.Text = Math.Tan(Double.Parse(tbResult.Text)).ToString();

        }


    }
}




;



