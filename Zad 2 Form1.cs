using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        List<string> list = new List<string>();
        int counter = 10;
        string path = "list.txt";


        public Form1()
        {
            InitializeComponent();
        }

        private void load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {


                    list.Add(line);
                }
                lb.DataSource = null;
                lb.DataSource = list;
                lb.Hide();

            }

        }

        private void label1_Click(object sender, EventArgs e)
        {
            string a = lb.SelectedItem.ToString();
            string b = tb1.Text;
            if (a == b)
            {
                MessageBox.Show("You guessed right");
            }
            else
            {
                label.Text = (--counter).ToString();
            }
        }

        private void Check(object sender, EventArgs e)
        {
            bool found = false;
            string word = lb.SelectedItem.ToString();
            string letter = tb.Text;
            if (word.Substring(0, word.Length).Contains(letter))
            {
                found = true;
            }
            else
            {
                found = false;
                label.Text = (--counter).ToString();
                if (counter == 0)
                {
                    MessageBox.Show("You lost");
                    counter = 10;
                }
            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            Random generator = new Random();
            int var = lb.Items.Count;
            int i = generator.Next(0, var);
            lb.SetSelected(i, true);
            string a = lb.SelectedItem.ToString();
            label1.Text = "Letters count: " + a.Length.ToString();

        }

      

     
    }
}

